## Automated Tests of Expedia.com

### Basic Usage
1. Navigate to the project's root directory
2. Type `python run_cycles.py`
3. Wait for UI tests to finish
4. Navigate to the `test_case_results` directory
    * Directory contains screenshots of different search results
    * Directory contains text file of search results
    * File names are intuitive and include dates
    
### Technical Usage
To extend the current project, the following steps must be taken:
1. Create a `page` script in the `pages` directory and extend the `base_page` and 
`base_element` classes to instantiate the elements relevant to the test cases to
be created
2. Add relevant test data to the `input_data` spreadsheet if applicable.  Also, if
necessary, you can add methods to the `test_data_factory` script if there is required 
data manipulation not currently addressed.
3. Create the test case by creating a new script in the `test_cases` directory. 
If variables and methods are named in a domain specific language, these test 
cases should be comprehensible to non-technical users.
4. Define how the results are to be captured by using or extending the `result_factory`
script in the `test_case_results` directory.  Currently there is a write to file, and 
screenshot method.  Note, the results of test cases will land in this directory.
5. Add result method calls to the test case scripts.
6. Create a cycle or add to an existing one by using the files in the `test_cycle`
directory.  Build the cycle by defining functions that import the different test cases
in the order you would like them run.
7. Create a script in the root directory that invokes the cycle you created, then run
it from the command line.  This will run all the test cases defined in the cycle and 
consequently collect the test results and record them in the `test_case_results` directory.
    
### Code Explanation
#### `pages` directory
The pages directory contains the base classes for pages and elements. The element class
contains the building blocks for HTML elements, including an ambiguous `find_by`, 
built in implicit waits, and basic user actions like click, input text, etc.

The named page classes inherit from the base page class and include all relevant 
elements for the tests to be performed on that page.

The main highlight of this directory is the lack of needing to use a `sleep` or
`wait` function for any elements.  Telling the program to wait for a period of time
is not best practice, so the class for elements has the implicit wait built in to
any element that extends it.  Therefore, each element we define in our `pages` directory
will be pre-built with a `wait` function that only waits until the element is accessible.

#### `test_cases` directory
This directory is intended to hold each page's test script.  The test scripts will
instantiate the page under test for that particular test case and then run through the
test steps as specified by whoever is working in that file.

If the `page` objects and their methods are well defined, it should be easy to create
test scripts for non-technical or semi-technical users.  This way, business analysts, 
customers, project managers, or junior users can contribute to building test cases
for the automated test suite with little training.

#### `test_cycles` directory
This directory will contain the files that specify which test cases will be run for
any given cycle.  The test cases are defined in this file as functions containing 
import statements for the relevant test case.

With well named test cases, these files should be easy to build for non-technical users.
This way, business analysts, project managers, or customers can easily define their
acceptance tests which will help the project team understand better what needs to be 
done to satisfy customer requirements.

#### `test_data` directory
The test data directory contains files that impact the different inputs used by the test
cases.  The `input_data` file is an Excel spreadsheet containing lists of relevant 
data types such as locations, dates, and activities.  The `test_data_factory` file
contains methods that return data randomly pulled from the lists in the `test_data` 
spreadsheet.

It is important to version control test data to help diagnose test failures.  Furthermore
it was decided that the input data for test cases should be easily accessible to non-technical
users.  Therefore, a self-explanatory spreadsheet was used so that the types of input could be created
or extended by business users as well as programmers or testers.

#### `test_case_results` directory
Finally, this directory will contain results of test cases that are run.  After each
test case is run, there will be new files in this directory containing different kinds
of results.  The `result_factory` contains methods for displaying results such as 
writing to a text file or taking screenshots.  These methods are invoked from the different
test case scripts so the test case designers can customize how results are displayed.

### Future improvements
This section details some areas in which this project could be improved for easier 
maintainability, faster test case generation, and better results.

#### Page factory
The biggest weakness in the current project is the hand coding of each page.  Creating
a factory pattern for the pages would be ideal.  To implement this, there would be 
one function that built properties that took inputs from either a text file or spreadsheet.
The file would contain a column for element selector (id, xpath, css selector), and the
adjacent column would contain the value, and a third column would contain the variable 
names for those elements.  The script would then loop through the file and create 
page objects on the fly.  The advantage here would be that the level of technical
expertise needed to create page objects to be tested is low. Users could simply make a 
list of objects by copy/pasting data gathered from `inspect element` and then commit
the spreadsheet to the repository.

#### Project accessible on web page
Instead of using spreadsheets to control input data and sending test results to a 
directory file, it would be best perhaps hold this data in a database and extend the
functionality onto a web page.  This way, non-technical users could simply go to a 
URL to see results, or use a web form to create test data and page objects in a more
user friendly way.

