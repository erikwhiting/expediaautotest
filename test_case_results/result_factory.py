import pyautogui
import datetime


def make_results_picture(test_case):
    now = datetime.datetime.now()
    ss_file_name = test_case + '_screenshot_' + now.strftime("%m-%d-%Y_%H-%M") + '.jpg'
    pic = pyautogui.screenshot()
    pic.save(".\\test_case_results\\" + ss_file_name)


def write_results_to_text(test_case, results_data):
    now = datetime.datetime.now()
    txt_file_name = test_case + '_text_' + now.strftime("%m-%d-%Y_%H-%M") + '.txt'
    results_file = open(txt_file_name, 'w')
    results_file.write(results_data)
