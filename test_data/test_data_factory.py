import openpyxl
import random

def open_test_data():
    try:
        td = openpyxl.load_workbook('.\\test_data\\input_data.xlsx', data_only=True)
    except IOError:
        td = "Cannot find 'input_data.xlsx' please check the 'test_data' directory"

    return td


def random_location():
    td = open_test_data()
    location_sheet = td['Location']
    data_range = location_sheet.max_row
    random_cell = random.randint(1, data_range)
    location = location_sheet.cell(row=random_cell, column=1).value
    return location


def random_date():
    td = open_test_data()
    date_sheet = td['Date']
    data_range = date_sheet.max_row
    random_cell = random.randint(1, data_range - 7)  # So arrive can be 7 days later without going past data range
    depart = date_sheet.cell(row=random_cell, column=1).value
    arrive = date_sheet.cell(row=random_cell+7, column=1).value
    return str(depart), str(arrive)


def random_activity():
    td = open_test_data()
    activity_sheet = td['Activity']
    data_range = activity_sheet.max_row
    random_cell = random.randint(1, data_range)
    activity = activity_sheet.cell(row=random_cell, column=1).value
    return activity


