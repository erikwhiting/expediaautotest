from selenium.webdriver.common.by import By

from .base_element import BaseElement
from .base_page import BasePage


class DiscoverPage(BasePage):
    url = 'https://www.expedia.com/'

    @property
    def discover_tab(self):
        locator = (By.ID, 'tab-openSearch-tab-hp')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def discover_search(self):
        locator = (By.ID, 'openSearch-searchQuery-hp-openSearch')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def discover_search_button(self):
        locator = (By.XPATH, '//*[@id="gcw-open-search-form-hp-openSearch"]/button')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def search_results(self):
        locator = (By.ID, 'nautilus-results')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @classmethod
    def get_text_results(cls, search_results):
        results = []
        links = search_results.find_elements_by_tag_name('a')
        for item in search_results:
            results.append(item.getAttribute('innerText'))
        return results
