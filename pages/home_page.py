from selenium.webdriver.common.by import By

from .base_element import BaseElement
from .base_page import BasePage


class HomePage(BasePage):
    url = 'https://www.expedia.com/'

    #flight stuff

    @property
    def flight_tab(self):
        locator = (By.ID, 'tab-flight-tab-hp')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def flight_origin(self):
        locator = (By.ID, 'flight-origin-hp-flight')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def flight_destination(self):
        locator = (By.ID, 'flight-destination-hp-flight')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def depart_date(self):
        locator = (By.ID, 'flight-departing-hp-flight')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def return_date(self):
        locator = (By.ID, 'flight-returning-hp-flight')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def flight_search_button(self):
        locator = (By.XPATH, '//*[@id="gcw-flights-form-hp-flight"]/div[8]/label/button')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    # hotel elements

    @property
    def hotel_tab(self):
        locator = (By.ID, 'tab-hotel-tab-hp')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def going_to(self):
        locator = (By.ID, 'hotel-destination-hp-hotel')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def check_in(self):
        locator = (By.ID, 'hotel-checkin-hp-hotel')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def check_out(self):
        locator = (By.ID, 'hotel-checkout-hp-hotel')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def hotel_search_button(self):
        locator = (By.XPATH, '//*[@id="gcw-hotel-form-hp-hotel"]/div[9]/label/button')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    # bundle elements

    @property
    def bundle_and_save_tab(self):
        locator = (By.ID, 'tab-package-tab-hp')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def bundle_origin(self):
        locator = (By.ID, 'package-origin-hp-package')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def bundle_destination(self):
        locator = (By.ID, 'package-destination-hp-package')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def bundle_departing(self):
        locator = (By.ID, 'package-departing-hp-package')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def bundle_returning(self):
        locator = (By.ID, 'package-returning-hp-package')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def bundle_search_button(self):
        # unlike the other search buttons, this one has an ID
        locator = (By.ID, 'search-button-hp-package')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    # cars elements

    @property
    def cars_tab(self):
        locator = (By.ID, 'tab-car-tab-hp')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def cars_picking_up(self):
        locator = (By.ID, 'car-pickup-hp-car')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def cars_dropping_off(self):
        locator = (By.ID, 'car-dropoff-hp-car')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def cars_pick_up_date(self):
        locator = (By.ID, 'car-pickup-date-hp-car')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def cars_drop_off_date(self):
        locator = (By.ID, 'car-dropoff-date-hp-car')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def cars_search_button(self):
        locator = (By.XPATH, '//*[@id="gcw-cars-form-hp-car"]/div[7]/label/button')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    # cruises elements

    @property
    def cruises_tab(self):
        locator = (By.ID, 'tab-cruise-tab-hp')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def cruises_destination(self):
        locator = (By.ID, 'cruise-destination-hp-cruise')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def cruises_destination_options(self):
        locator = (By.CSS_SELECTOR, '#cruise-destination-hp-cruise > optgroup:nth-child(2) > option:nth-child(1)')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def cruises_departure_month(self):
        locator = (By.ID, 'cruise-departure-month-hp-cruise')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def cruises_departure_options(self):
        locator = (By.CSS_SELECTOR, '#cruise-departure-month-hp-cruise > option:nth-child(8)')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def cruises_search_button(self):
        locator = (By.XPATH, '//*[@id="gcw-cruises-form-hp-cruise"]/div[3]/label/button')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    # things to do elements

    @property
    def things_to_do_tab(self):
        locator = (By.ID, 'tab-activity-tab-hp')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def things_destination(self):
        locator = (By.ID, 'activity-destination-hp-activity')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def things_from_date(self):
        locator = (By.ID, 'activity-start-hp-activity')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def things_to_date(self):
        locator = (By.ID, 'activity-end-hp-activity')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def things_search_button(self):
        locator = (By.XPATH, '//*[@id="gcw-activities-form-hp-activity"]/div[5]/label/button')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def vacation_rentals_tab(self):
        locator = (By.ID, 'tab-vacation-rental-tab-hp')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def vacation_destination(self):
        locator = (By.ID, 'hotel-destination-hp-vacationRental')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def vacation_check_in_date(self):
        locator = (By.ID, 'hotel-checkin-hp-vacationRental')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def vacation_check_out_date(self):
        locator = (By.ID, 'hotel-checkout-hp-vacationRental')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def vacation_search_button(self):
        locator = (By.XPATH, '//*[@id="gcw-hotel-form-hp-vacationRental"]/div[5]/label/button')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def discover_tab(self):
        locator = (By.ID, 'tab-openSearch-tab-hp')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def discover_search(self):
        locator = (By.ID, 'openSearch-searchQuery-hp-openSearch')
        return BaseElement(self.driver, by=locator[0], value=locator[1])

    @property
    def discover_search_button(self):
        locator = (By.XPATH, '//*[@id="gcw-open-search-form-hp-openSearch"]/button')
        return BaseElement(self.driver, by=locator[0], value=locator[1])
