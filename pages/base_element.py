from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class BaseElement(object):
    def __init__(self, driver, value, by):
        self.driver = driver
        self.value = value
        self.by = by
        self.locator = (self.by, self.value)

        self.web_element = None
        self.find()

    def find(self):
        # code for ambiguous find_by_*
        element = WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located(locator=self.locator))
        self.web_element = element
        return None

    def click(self):
        element = WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable(locator=self.locator))
        element.click()
        return None

    def input_text(self, txt):
        self.web_element.send_keys(txt)
        return None

    def clear(self):
        element = WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable(locator=self.locator))
        element.clear()
        return None

    def clear_text(self):
        self.web_element.send_keys(Keys.CONTROL + "a")
        self.web_element.send_keys(Keys.DELETE)
        return None

    def drop_down_select(self, selection_index):
        element = WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable(locator=self.locator))
        element.select_by_index(selection_index)

    @property
    def text(self):
        text = self.web_element.text
        return text
