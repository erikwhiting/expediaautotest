from random import randint

from selenium import webdriver

from pages.home_page import HomePage
from test_case_results.result_factory import make_results_picture
import test_data.test_data_factory as td

# setup

browser = webdriver.Chrome()
expedia = HomePage(browser)
browser.maximize_window()


# flight tab tests
expedia.go()
expedia.flight_tab.click()
expedia.flight_origin.input_text(td.random_location())
expedia.flight_destination.input_text(td.random_location())
f_depart, f_arrive = td.random_date()
expedia.depart_date.clear_text()
expedia.depart_date.input_text(f_depart)
expedia.return_date.clear_text()
expedia.return_date.input_text(f_arrive)
expedia.flight_search_button.click()
make_results_picture('HomePage_Flight')


# # hotel tab tests
expedia.go()
expedia.hotel_tab.click()
expedia.going_to.input_text(td.random_location())
h_check_in, h_check_out = td.random_date()
expedia.check_in.clear_text()
expedia.check_in.input_text(h_check_in)
expedia.check_out.clear_text()
expedia.check_out.input_text(h_check_out)
expedia.hotel_search_button.click()
make_results_picture('HomePage_Hotel')


# bundle and save tests
expedia.go()
expedia.bundle_and_save_tab.click()
expedia.bundle_origin.input_text(td.random_location())
expedia.bundle_destination.input_text(td.random_location())
b_depart, b_return = td.random_date()
expedia.bundle_departing.clear_text()
expedia.bundle_departing.input_text(b_depart)
expedia.bundle_returning.clear_text()
expedia.bundle_returning.input_text(b_return)
expedia.bundle_search_button.click()
make_results_picture('HomePage_Bundle_And_Save')


# # cars tests
expedia.go()
expedia.cars_tab.click()
expedia.cars_picking_up.input_text(td.random_location())
expedia.cars_dropping_off.input_text(td.random_location())
c_pick_up, c_drop_off = td.random_date()
expedia.cars_pick_up_date.clear_text()
expedia.cars_pick_up_date.input_text(c_pick_up)
expedia.cars_dropping_off.clear_text()
expedia.cars_dropping_off.input_text(c_drop_off)
expedia.cars_search_button.click()
make_results_picture('HomePage_Cars')


# cruises tests
expedia.go()
expedia.cruises_tab.click()
expedia.cruises_destination.click()
expedia.cruises_destination_options.click()
expedia.cruises_departure_month.click()
expedia.cruises_departure_options.click()
expedia.cruises_search_button.click()
make_results_picture('HomePage_Cruises')

# things to do tests
expedia.go()
expedia.things_to_do_tab.click()
expedia.things_destination.input_text(td.random_location())
t_start, t_end = td.random_date()
expedia.things_from_date.input_text(t_start)
expedia.things_to_date.input_text(t_end)
expedia.things_search_button.click()
make_results_picture('HomePage_ThingsToDo')

# vacation rentals tests
expedia.go()
expedia.vacation_rentals_tab.click()
expedia.vacation_destination.input_text(td.random_location())
v_check_in, v_check_out = td.random_date()
expedia.vacation_check_in_date.clear_text()
expedia.vacation_check_in_date.input_text(v_check_in)
expedia.vacation_check_out_date.clear_text()
expedia.vacation_check_out_date.input_text(v_check_out)
expedia.vacation_search_button.click()
make_results_picture('HomePage_Vacation')

browser.quit()
