from random import randint

from selenium import webdriver

from pages.discover_page import DiscoverPage
from test_case_results.result_factory import make_results_picture, write_results_to_text
import test_data.test_data_factory as td



# setup

browser = webdriver.Chrome()
discover = DiscoverPage(browser)
browser.maximize_window()
discover.go()

# main test

discover.discover_tab.click()
activity = td.random_activity()
location = td.random_location()
activity_near_location = "{} near {}".format(activity, location)
discover.discover_search.input_text(activity_near_location)
discover.discover_search_button.click()
search_results = discover.search_results
text_results = discover.get_text_results(search_results)
make_results_picture("discovery_{}".format(activity_near_location))
write_results_to_text("discovery_()".format(activity_near_location), text_results)

